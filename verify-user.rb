#!/usr/bin/env ruby
# frozen_string_literal: true

require 'ostruct'
require 'time'
require 'etc'
require 'tty-prompt'
require 'tty-screen'
require 'toml'
require 'discourse_api'
require 'httparty'
require 'json'

VERSION = 0.2
TARGET_TIME = '05-01-2023'
def sep
  puts
  puts '~' * TTY::Screen.width
  puts
end

prompt = TTY::Prompt.new

config = nil
system 'clear'
system 'toilet -F gay -f script Verify User'
sep

if File.file?("#{Dir.home}/.config/dsc.toml")
  f = File.open("#{Dir.home}/.config/dsc.toml", 'r')
  data = f.read
  parser = TOML::Parser.new(data)
  config = parser.parsed

else
  puts 'Config not found'
  config = { 'settings' => {} }
  config['settings']['username'] = prompt.ask(' Username:')
  config['settings']['key'] = prompt.ask(' Api Key?')
  config['settings']['baseurl'] = prompt.ask(' Discourse Url:') do |q|
    q.validate(%r{^(http|https)://[^ "]+$})
    q.messages[:valid?] = 'Invalid URL'
  end
end
# A Custom API client for our use
# instance methods are high level actions
# class methods are thin abstractions to discourse api methods
class BrownOwlDiscourseAPIClient
  include HTTParty

  def initialize(config)
    @config = config
    self.class.base_uri @config['settings']['baseurl']
    base_url = @config['settings']['baseurl']
    @headers = {
      'Api-Key' => @config['settings']['key'],
      'Api-Username' => @config['settings']['username'],
      'User-Agent' => "Mozilla/5.0 (compatible; BrownOwlAPI #{VERSION} #{base_url}/u/#{config['settings']['baseurl']} ",
      'Content-Type' => 'application/json'
    }
  end

  def self.add_users(hdrs, group_id, users)
    user_csv = ''
    users.each do |e|
      e += ','
      user_csv += e
    end
    jsuname = { 'usernames' => user_csv }
    rqbody = JSON.generate(jsuname)
    pp hdrs
    resp = put("/groups/#{group_id}/members.json", body: rqbody, headers: hdrs)
    if resp.code == 200
      true
    else
      pp resp.body
      false
    end
  end

  def addusers(gid, users)
    self.class.add_users(@headers, gid, users)
  end
end

client = DiscourseApi::Client.new(config['settings']['baseurl'])
client.api_username = config['settings']['username']
client.api_key = config['settings']['key']
# puts client.topics_by(client.api_username)
verified_group = client.group('Verified')
vg_id = verified_group['group']['id']
puts vg_id
target = prompt.ask(' Username to be verified')
raw_user = nil
begin
  raw_user = client.user(target)
rescue DiscourseApi::DiscourseError => e
  puts e.response.body['errors']
  abort('Something went wrong')
end
user = {}
user['posts'] = raw_user['post_count']
user['joined'] = raw_user['created_at']
user['name'] = raw_user['username']
user['seen'] = raw_user['last_seen_at']
user['last_post'] = raw_user['last_posted_at']
our_user = OpenStruct.new(user)

puts "User #{our_user.name}\n joined #{our_user.joined}
was seen on #{our_user.seen} and posted on #{our_user.last_post}\n Has #{our_user.posts} posts "

target_ts = Time.parse(TARGET_TIME)
user_lastpost_ts = Time.parse(our_user.last_post)
abort('Cannot verify') if our_user.posts < 1

abort('Appears invalid') if target_ts > user_lastpost_ts
myclient = BrownOwlDiscourseAPIClient.new(config)

if prompt.yes?(" Verify this User #{target}")
  users = [target]
  if myclient.addusers(vg_id, users)
    puts 'Success'
  else
    puts 'fail'
  end
end
